# README #

Este README es para responder las preguntas de la evaluaci�n asi como la puesta en marcha de la aplicaci�n.

# Repositorio de evaluaci�n  #

Este repositorio fue hecho con la finalidad de ayudar en el proceso de evaluaci�n para la vacante de desarrollador en Python.

# Preguntas y respuestas de la evaluaci�n #

## Secci�n 1: UML. ##

### a) Para el proceso de b�squeda y descarga de facturas. Genere los diagramas UML correspondientes (Secuencia y Clases) para la versi�n web. ###

Diagrama de clases:

## [Descargar proyecto en NClass](http://giltamayo.com:8000/invoices/ProteinasOleicos.ncp) ##

![Alt text](http://giltamayo.com:8000/invoices/diagramaclases.png "Diagrama de clases")

Diagrama de secuencia:

## [Descargar diagrama en ms visio](http://giltamayo.com:8000/invoices/DescargarFacturas.vsdx) ##

![Alt text](http://giltamayo.com:8000/invoices/diagramaclases.png "Diagrama de clases")

### b) Genere el diagrama de entidad relaci�n (ER) para ambas aplicaciones. ###

## [Descargar diagrama en ms visio](http://giltamayo.com:8000/invoices/ER.vsdx) ##

![Alt text](http://giltamayo.com:8000/invoices/ER.png "Diagrama ER")

### c) Genere un diagrama que ejemplifique la arquitectura utilizada en ambas aplicaciones. ###

El proyecto consta de una API REST en flask para que se pueda utilizar tanto en aplicaciones WEB como en m�viles, se hizo uso del mapeador ORM SQLAlchemy, esta API consta con la capa modelo donde se definen las tablas y columnas, capa controlador que contiene la logica de la aplicaci�n, capa esquema que define el formato de salida de la API y se le anexo una capa templates donde se incluyen los html que intectuan con la API.

~~~~  
        - App   
            + models
                * customer.py
                * invoice.py
                * user.py
            + controllers
                * customer.py
                * invoice.py
                * user.py
            + schemas
                * customer.py
                * invoice.py
                * user.py
            + templates
            	* index.html
~~~~

![Alt text](http://giltamayo.com:8000/invoices/esquema.png "Esquema")

## Secci�n 2: UI. ##

### Con base en el problema planteado en la secci�n 1, haga un bosquejo de la interfaz gr�fica para las aplicaciones Web y M�vil. ###

Aplicaci�n Web:

## [Wireframe WEB](https://app.moqups.com/gilbert.i.tamayo@gmail.com/P20SRyoibg/view/page/aa9df7b72) ##

![Alt text](http://giltamayo.com:8000/invoices/web.png "Web")

Aplicaci�n M�vil(Pantalla de filtrado y generaci�n de la grilla):

## [Wireframe M�vil Filtrado](https://app.moqups.com/gilbert.i.tamayo@gmail.com/P20SRyoibg/view/page/a3472168a) ##

![Alt text](http://giltamayo.com:8000/invoices/mobilefiltro.png "Filtrado")

## [Wireframe M�vil Resultados](https://app.moqups.com/gilbert.i.tamayo@gmail.com/P20SRyoibg/view/page/a6e61e500) ##

![Alt text](http://giltamayo.com:8000/invoices/mobilegrid.png "Resultados de b�squeda")

### Para la aplicaci�n web maquete el bosquejo realizado. ###

Para el maquetado se hizo uso de html5, bootstrap 4, jquery, ajax y otros complementos como lo es [Datatables](https://datatables.net/) y [fontawesome](http://fontawesome.io/icons/)

WEB

Se finaliz� el maquetado de acuerdo al wireframe y para cuesti�n de funcionamiento, se hace el filtrado de acuerdo a los criterios establecidos por el usuario.

## [Ver Demo de la aplicaci�n](http://giltamayo.com:8000/) ##

API REST con Python Flask

Se realizaron los EndPoint para add/Edit/Delete/List/ListByID para usuarios, clientes y facturas. Tambien se agreg� el endpoint para listar las facturas de acuerdo a los criterios del Folio, usuario, cliente y fecha.

## [Ver como consumir los EndPoints(Postman)](https://documenter.getpostman.com/view/268102/collection/6Z6rqQ4) ##

## Secci�n 3: SQL. ##

### Con base en el diagrama de entidad relaci�n planteado en el inciso b) de la secci�n 1, escriba el query necesario para: ###

### a) Recuperar los registros de las facturas, dado mas de un criterio de busqueda ###

SELECT referenceno,deliverydate,urlxml,urlpdf  FROM INVOICE WHERE  referenceno like �%� + @criterioreferenceno + �%� AND deliverydate = �2017-06-06�

## Secci�n 4: Base de Datos. ##

### Explique de forma clara y simple los siguientes conceptos acerca de base de datos: ###

### a) Trigger. ###

Procedimiento almacenado que se ejecuta cuando se actualizan ciertos datos de una tabla en particulas

### b) Forma Normal. ###

Es una norma para organizar las tablas de acuerdo a unas reglas establecidas, esta norma nos ayuda a eliminar la redundancia y proteger la informaci�n.

### c) Indice. ###

Identificador por el cual funciona para identificar las filas de las columnas, estas se crean cuando se agregan llaves primarias y secundarias.

### d) Procedimiento Almacenado. ###

Se almacena en la base de datos y traen varias funciones, como realizar transacciones y ejecutar consultas. Tienen como ventaja que si se usa adecuadamente suelen ahorrarse en tiempo de ejecuci�n.

### e) Llave Foranea. ###

Es una columna llave que relaciona una tabla con otra.

## Secci�n 5: Patrones. ##

### Explique de forma clara y simple los siguientes conceptos acerca de los patrones de dise�o java: ###

### a) Factory. ###

Es un patr�n que consiste en la creaci�n de objetos para una clase en espec�fico para luego instanciar alguno de esos objetos de acuerdo a que se requiera desplegar. Un ejemplo seria los m�todos.

### b) Composite. ###

Este patr�n simplifica objetos que tengan similitudes y encapsularlos en una interfaz en com�n, tambi�n esa interfaz en com�n puede ser parte de alg�n otra clase m�s compleja.

### c) State. ###

Se usa cuando un objeto tiene cierta reacci�n de acuerdo al estado en la que se encuentra.
