from app.database import db
 
class Invoice(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    referenceno = db.Column(db.String(50), unique=True,nullable = False)
    deliverydate = db.Column(db.DateTime())
    userid = db.Column(db.Integer, db.ForeignKey('user.id'))
    customerid = db.Column(db.Integer, db.ForeignKey('customer.id'))
    urlxml = db.Column(db.String(250), nullable=True)
    urlpdf = db.Column(db.String(250), nullable=True)

    user = db.relationship('User', foreign_keys=[userid])
    customer = db.relationship('Customer', foreign_keys=[customerid])
    
    
 
    def __init__(self,  referenceno,  deliverydate, userid, customerid, urlxml, urlpdf):
        self.referenceno = referenceno
        self.deliverydate = deliverydate
        self.userid = userid
        self.customerid = customerid
        self.urlxml = urlxml
        self.urlpdf = urlpdf
         
    def add(self, resource):
        db.session.add(resource)
        return db.session.commit()
 
    def update(self):
        return db.session.commit()
 
    def delete(self, resource):
        db.session.delete(resource)
        return db.session.commit()