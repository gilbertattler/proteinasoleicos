from werkzeug.security import generate_password_hash, check_password_hash
from app.database import db
 
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50),nullable = False)
    passhash = db.Column(db.String(256),nullable = False)
    firstname = db.Column(db.String(50), nullable=True)
    lastname = db.Column(db.String(50), nullable=True)
    
 
    def __init__(self,  username,  firstname, lastname,  password):
        self.username = username
        self.firstname = firstname.lower()
        self.lastname = lastname.lower()
        self.set_password(password)
         
    def add(self, resource):
        db.session.add(resource)
        return db.session.commit()
 
    def update(self):
        return db.session.commit()
 
    def delete(self, resource):
        db.session.delete(resource)
        return db.session.commit()
     
    def set_password(self, password):
        self.passhash = generate_password_hash(password)
   
    def check_password(self, password):
        return check_password_hash(self.passhash, password)