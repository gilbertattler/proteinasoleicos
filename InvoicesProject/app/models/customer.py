from app.database import db
 
class Customer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50),nullable = False)
    email = db.Column(db.String(50),nullable = False)
    rfc = db.Column(db.String(13), nullable=False, unique=True)
    
 
    def __init__(self,  name,  email, rfc):
        self.name = name.lower()
        self.email = email
        self.rfc = rfc.lower()
         
    def add(self, resource):
        db.session.add(resource)
        return db.session.commit()
 
    def update(self):
        return db.session.commit()
 
    def delete(self, resource):
        db.session.delete(resource)
        return db.session.commit()