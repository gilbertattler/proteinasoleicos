from flask import Blueprint, request, jsonify, make_response
from flask_restful import Api, Resource, reqparse, fields, marshal_with
from app.database import db
from app.models.customer import Customer
from app.schemas.customer import CustomerSchema
from sqlalchemy.exc import SQLAlchemyError
from marshmallow import ValidationError

customer = Blueprint('customer', __name__)
 
schema = CustomerSchema()

api = Api(customer)

class CreateListCustomer(Resource):
    
    def get(self):
        customer_query = Customer.query.all()
        results = schema.dump(customer_query, many=True)['data']
        return results
 
    def post(self):
        raw_dict = request.get_json(force=True)
        try:
            schema.validate(raw_dict)
            request_dict = raw_dict['data']['attributes']
            customer = Customer(request_dict['name'], request_dict['email'], request_dict['rfc'])
            customer.add(customer)
            query = Customer.query.get(customer.id)
            results = schema.dump(query)['data']
            return results, 201
 
        except ValidationError as err:
            resp = jsonify({"error": err.messages})
            resp.status_code = 403
            return resp
 
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            resp.status_code = 403
            return resp
 
 
class GetUpdateDeleteCustomer(Resource):
    
    def get(self, id):
        customer_query = Customer.query.get_or_404(id)
        result = schema.dump(customer_query)['data']
        return result
 
    def patch(self, id):
        customer = Customer.query.get_or_404(id)
        raw_dict = request.get_json(force=True)
        try:
            schema.validate(raw_dict)
            request_dict = raw_dict['data']['attributes']
            for key, value in request_dict.items():
                setattr(customer, key, value)
 
            customer.update()
            return self.get(id)
 
        except ValidationError as err:
            resp = jsonify({"error": err.messages})
            resp.status_code = 401
            return resp
 
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            resp.status_code = 401
            return resp
 
    def delete(self, id):
        customer = Customer.query.get_or_404(id)
        try:
            delete = customer.delete(customer)
            response = make_response()
            response.status_code = 204
            return response
 
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            resp.status_code = 401
            return resp

#Mapear las clases para los endpoints de la API
api.add_resource(CreateListCustomer, '.json')
api.add_resource(GetUpdateDeleteCustomer, '/<int:id>.json')
