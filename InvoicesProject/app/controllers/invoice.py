from flask import Blueprint, request, jsonify, make_response
from flask_restful import Api, Resource, reqparse, fields, marshal_with
from app.database import db
from app.models.invoice import Invoice
from app.models.user import User
from app.schemas.invoice import InvoiceSchema
from app.schemas.user import UserSchema
from sqlalchemy.exc import SQLAlchemyError
from marshmallow import ValidationError

invoice = Blueprint('invoice', __name__)
 
schema = InvoiceSchema()

api = Api(invoice)

class CreateListInvoice(Resource):
    
    def get(self):
        invoice_query = Invoice.query.all()
        results = schema.dump(invoice_query, many=True)['data']
        return results
 
    def post(self):
        raw_dict = request.get_json(force=True)
        try:
            schema.validate(raw_dict)
            request_dict = raw_dict['data']['attributes']
            invoice = Invoice(request_dict['referenceno'], request_dict['deliverydate'], request_dict['userid'], request_dict['customerid'], request_dict['urlxml'], request_dict['urlpdf'])            
            invoice.add(invoice)
            query = Invoice.query.get(invoice.id)
            results = schema.dump(query)['data']
            return results, 201
 
        except ValidationError as err:
            resp = jsonify({"error": err.messages})
            resp.status_code = 403
            return resp
 
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            resp.status_code = 403
            return resp
 
class CreateListByInvoice(Resource):
    
    def get(self, referenceno,customerid,userid,deliverydate):
        invoice_query = Invoice.query.filter(Invoice.referenceno.like("%"+("" if referenceno == "None" else referenceno)+"%"), Invoice.customerid == (customerid if customerid > 0 else Invoice.customerid), Invoice.userid == (userid if userid > 0 else Invoice.userid), Invoice.deliverydate == (Invoice.deliverydate if deliverydate == "None" else deliverydate)).all()
        results = schema.dump(invoice_query, many=True)['data']
        return results
 
class GetUpdateDeleteInvoice(Resource):
    
    def get(self, id):
        invoice_query = Invoice.query.get_or_404(id)
        result = schema.dump(invoice_query)['data']
        return result
 
    def patch(self, id):
        invoice = Invoice.query.get_or_404(id)
        raw_dict = request.get_json(force=True)
        try:
            schema.validate(raw_dict)
            request_dict = raw_dict['data']['attributes']
            for key, value in request_dict.items():
                setattr(invoice, key, value)
 
            invoice.update()
            return self.get(id)
 
        except ValidationError as err:
            resp = jsonify({"error": err.messages})
            resp.status_code = 401
            return resp
 
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            resp.status_code = 401
            return resp
 
    def delete(self, id):
        invoice = Invoice.query.get_or_404(id)
        try:
            delete = invoice.delete(invoice)
            response = make_response()
            response.status_code = 204
            return response
 
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            resp.status_code = 401
            return resp

#Mapear las clases para los endpoints de la API
api.add_resource(CreateListInvoice, '.json')
api.add_resource(CreateListByInvoice, '/<string:referenceno>/<int:customerid>/<int:userid>/<string:deliverydate>.json')
api.add_resource(GetUpdateDeleteInvoice, '/<int:id>.json')
