from flask import Blueprint, request, jsonify, make_response
from flask_restful import Api, Resource, reqparse, fields, marshal_with
from app.database import db
from app.models.user import User
from app.schemas.user import UserSchema
from sqlalchemy.exc import SQLAlchemyError
from marshmallow import ValidationError

user = Blueprint('user', __name__)
 
schema = UserSchema()

api = Api(user)

class CreateListUser(Resource):
    
    def get(self):
        user_query = User.query.all()
        results = schema.dump(user_query, many=True)['data']
        return results
 
    def post(self):
        raw_dict = request.get_json(force=True)
        try:
            schema.validate(raw_dict)
            request_dict = raw_dict['data']['attributes']

            if len(request_dict['password']) >= 5:
                user = User(request_dict['username'], request_dict['firstname'], request_dict['lastname'], request_dict['password'])
                user.add(user)
                query = User.query.get(user.id)
                results = schema.dump(query)['data']
                return results, 201
            else:
                resp = jsonify({"error": "La longitud de la clave debe ser mayor o igual a 5"})
                resp.status_code = 403
                return resp
 
        except ValidationError as err:
            resp = jsonify({"error": err.messages})
            resp.status_code = 403
            return resp
 
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            resp.status_code = 403
            return resp
 
 
class GetUpdateDeleteUser(Resource):
    
    def get(self, id):
        user_query = User.query.get_or_404(id)
        result = schema.dump(user_query)['data']
        return result
 
    def patch(self, id):
        user = User.query.get_or_404(id)
        raw_dict = request.get_json(force=True)
        try:
            schema.validate(raw_dict)
            request_dict = raw_dict['data']['attributes']
            for key, value in request_dict.items():
                setattr(user, key, value)
 
            user.update()
            return self.get(id)
 
        except ValidationError as err:
            resp = jsonify({"error": err.messages})
            resp.status_code = 401
            return resp
 
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            resp.status_code = 401
            return resp
 
    def delete(self, id):
        user = User.query.get_or_404(id)
        try:
            delete = user.delete(user)
            response = make_response()
            response.status_code = 204
            return response
 
        except SQLAlchemyError as e:
            db.session.rollback()
            resp = jsonify({"error": str(e)})
            resp.status_code = 401
            return resp

#Mapear las clases para los endpoints de la API
api.add_resource(CreateListUser, '.json')
api.add_resource(GetUpdateDeleteUser, '/<int:id>.json')
