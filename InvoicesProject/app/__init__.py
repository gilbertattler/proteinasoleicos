from flask import Flask, render_template, send_from_directory
from app.database import db
import os

def create_app(config_filename):
    app = Flask(__name__)

    app.config.from_object(config_filename)

    db.init_app(app)

    @app.route('/<path:filename>')
    def serve_static(filename):
        return send_from_directory(os.path.join('.', 'templates'), filename)

    @app.route('/')
    def index():
        return render_template('index.html')
    
    from app.controllers.user import user
    app.register_blueprint(user, url_prefix='/api/users')

    from app.controllers.customer import customer
    app.register_blueprint(customer, url_prefix='/api/customers')
    
    from app.controllers.invoice import invoice
    app.register_blueprint(invoice, url_prefix='/api/invoices')

    return app