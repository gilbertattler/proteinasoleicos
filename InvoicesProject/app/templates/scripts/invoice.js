(function () {
	'use strict';

	var setSelectUsers = function(){
	    $.ajax({
	        type: 'GET',
	        url: 'api/users.json',
	        dataType: 'json',
	        data: '{}',
	        contentType: 'application/json; charset=utf-8',
	        cache: false,
	        success: function (result) {
	            var optionHTML = '<option value="0" Selected>Seleccione</option>'
			    for (var i = 0; i < result.data.length; i++) {
			        optionHTML += 	'<option value="' + result.data[i].id + 
			        				'" >'+ result.data[i].attributes.firstname + ' ' 
			        				+ result.data[i].attributes.lastname +'</option>';
			    }
			    $('#userid').html(optionHTML);
	        },
	        error: function (request, status, error) {
	            console.info(error);
	        }
	    });
	};

	var setSelectCustomers = function(){
	    $.ajax({
	        type: 'GET',
	        url: 'api/customers.json',
	        dataType: 'json',
	        data: '{}',
	        contentType: 'application/json; charset=utf-8',
	        cache: false,
	        success: function (result) {
	            var optionHTML = '<option value="0" Selected>Seleccione</option>'
			    for (var i = 0; i < result.data.length; i++) {
			        optionHTML += 	'<option value="' + result.data[i].id + 
			        				'" >'+ result.data[i].attributes.name +'</option>';
			    }
			    $('#customerid').html(optionHTML);
	        },
	        error: function (request, status, error) {
	            console.info(error);
	        }
	    });
	};

	var setTableInvoice = function(){
		
	    $.ajax({
	        type: 'GET',
	        url: 'api/invoices.json',
	        dataType: 'json',
	        data: '{}',
	        contentType: 'application/json; charset=utf-8',
	        cache: false,
	        success: function (result) {
	            var dataset = []; 
			    for (var i = 0; i < result.data.length; i++) {
			     	dataset.push([
			     		result.data[i].attributes.referenceno,
			     		result.data[i].attributes.deliverydate,
			     		result.data[i].attributes.customer.data.attributes.name,
			     		result.data[i].attributes.customer.data.attributes.rfc,
			     		result.data[i].attributes.customer.data.attributes.email,
			     		result.data[i].attributes.user.data.attributes.firstname + ' ' + result.data[i].attributes.user.data.attributes.lastname,
			     		'<a class="pl-4 text-muted" href="#" onclick="download(' + result.data[i].attributes.urlxml + ', ' + result.data[i].attributes.urlpdf + ')"><i class="fa fa-cloud-download" aria-hidden="true"></i></<>'
			     	]);
			    }

	    	    $('#tableinvoices').DataTable(
					{
				        searching:false,
				        lengthChange:false,
				        language: {
					         url: 'https://cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json'
				    	},
				    	ordering: false,
				    	data: dataset
			    	} 
				);
	        },
	        error: function (request, status, error) {
	            console.info(error);
	        }
	    });
	};

	var setTableByInvoice = function(referenceno,customerid,userid,deliverydate){
		
	    $.ajax({
	        type: 'GET',
	        url: 'api/invoices/' + referenceno + '/' + customerid + '/' + userid + '/' + deliverydate + '.json',
	        dataType: 'json',
	        data: '{}',
	        contentType: 'application/json; charset=utf-8',
	        cache: false,
	        success: function (result) {
	            var dataset = []; 
	            var oTable = $('#tableinvoices').dataTable();
			    for (var i = 0; i < result.data.length; i++) {
			     	dataset.push([
			     		result.data[i].attributes.referenceno,
			     		result.data[i].attributes.deliverydate,
			     		result.data[i].attributes.customer.data.attributes.name,
			     		result.data[i].attributes.customer.data.attributes.rfc,
			     		result.data[i].attributes.customer.data.attributes.email,
			     		result.data[i].attributes.user.data.attributes.firstname + ' ' + result.data[i].attributes.user.data.attributes.lastname,
			     		result.data[i].attributes.urlxml
			     	]);
			    }
			
				oTable.fnClearTable();;
				if(dataset.length>0){
			        oTable.fnAddData(dataset);
			        oTable.fnDraw();					
				}
	        },
	        error: function (request, status, error) {
	            console.info(error);
	        }
	    });
	};

	var getListByInvoice = function(){
		var referenceno = $('input#referenceno').val();
		var customerid = $('select#customerid').val();
		var userid = $('select#userid').val();
		var deliverydate = $('input#deliverydate').val();
		setTableByInvoice((referenceno==''?'None':referenceno),customerid,userid,(deliverydate==''?'None':deliverydate));
	};

	var init = function(){
		setSelectUsers();
		setSelectCustomers();
		setTableInvoice();
		$('#btnSearch').click(function () {
			getListByInvoice();
	    })
	};

	$(document).ready(function() {
		init();
	});
})();

