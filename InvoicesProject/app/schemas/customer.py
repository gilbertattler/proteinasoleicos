from marshmallow_jsonapi import Schema, fields
from marshmallow import validate

class CustomerSchema(Schema):
 
    not_blank = validate.Length(min=1, error='Los campos no deben estar en blanco')

    id = fields.Integer(dump_only=True)   
    name = fields.String(validate=not_blank)
    email = fields.String(validate=not_blank)
    rfc = fields.String(validate=not_blank)
 
    # self links
    def get_top_level_links(self, data, many):
        if many:
            self_link = "/customers"
        else:
            self_link = "/customers/{}".format(data['id'])
        return {'self': self_link}
    class Meta:
        type_ = 'customer'