from marshmallow_jsonapi import Schema, fields
from marshmallow import validate

class UserSchema(Schema):
 
    not_blank = validate.Length(min=1, error='Los campos no deben estar en blanco')

    id = fields.Integer(dump_only=True)   
    username = fields.String(validate=not_blank)
    passhash = fields.String(validate=not_blank)
    firstname = fields.String(validate=not_blank)
    lastname = fields.String(validate=not_blank)
 
    # self links
    def get_top_level_links(self, data, many):
        if many:
            self_link = "/users"
        else:
            self_link = "/users/{}".format(data['id'])
        return {'self': self_link}
    class Meta:
        type_ = 'user'