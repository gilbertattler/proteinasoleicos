from marshmallow_jsonapi import Schema, fields
from marshmallow import validate
from app.schemas.user import UserSchema
from app.schemas.customer import CustomerSchema

class InvoiceSchema(Schema):
 
    not_blank = validate.Length(min=1, error='Los campos no deben estar en blanco')

    id = fields.Integer(dump_only=True)   
    referenceno = fields.String(validate=not_blank)
    deliverydate = fields.String(validate=not_blank)
    urlxml = fields.String(validate=not_blank)
    urlpdf = fields.String(validate=not_blank)
    userid = fields.Integer()
    customerid = fields.Integer()
    user = fields.Nested(UserSchema)
    customer = fields.Nested(CustomerSchema)
 
    # self links
    def get_top_level_links(self, data, many):
        if many:
            self_link = "/invoices"
        else:
            self_link = "/invoices/{}".format(data['id'])
        return {'self': self_link}
    class Meta:
        type_ = 'invoice'